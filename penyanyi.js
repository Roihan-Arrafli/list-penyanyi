const listPenyanyi = [
    {
        namapenyanyi: "Denny Caknan",
        genre: "Dangdut",
        asal: "Ngawi"
    },
    {
        namapenyanyi: "Tulus",
        genre: "Pop",
        asal: "Bukittinggi"
    },
    {
        namapenyanyi: "Ardhito Pramono",
        genre: "Jazz",
        asal: "Jakarta"
    },
    {
        namapenyanyi: "Happy Asmara",
        genre: "Dangdut",
        asal: "Kediri"
    },
    {
        namapenyanyi: "Afgan",
        genre: "Pop",
        asal: "Jakarta"
    },
    {
        namapenyanyi: "Rizky Febian",
        genre: "Pop",
        asal: "Bandung"
    }
];

for(let i = 0; i<listPenyanyi.length; i++){
    console.log(listPenyanyi[i]["namapenyanyi"], 
    ("lahir di " + listPenyanyi[i]["asal"]), 
    ("menyanyikan lagu " + listPenyanyi[i]["genre"]));
}

console.log("-cari-")
function cariPenyanyi(namapenyanyi) {
    for(let i = 0; i <listPenyanyi.length; i++){
        if (listPenyanyi[i]["namapenyanyi"] == namapenyanyi) {
            console.log(listPenyanyi[i]["namapenyanyi"],
            ("lahir di " + listPenyanyi[i]["asal"]),  
            ("menyanyikan lagu " + listPenyanyi[i]["genre"]));
        }
    }
}
cariPenyanyi("Denny Caknan");

console.log("-filter-");
function filterAsal(asal) {
    for(let i = 0; i < listPenyanyi.length; i++){
        if (listPenyanyi[i]["asal"] == asal) {
            console.log(listPenyanyi[i]["namapenyanyi"]);
        }
    }
}
filterAsal("Jakarta")